#ifndef TP_MOTION__H
#define TP_MOTION__H
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.1415926535

typedef enum
{
	NO_ERROR,
	PASS_INPUT_POINT_ERROR,
	POS_INPUT_POINT_ERROR,
	MOVR_LENGTH_ZERO,
	VEL_PARAM_ERROR,
	OPENFOLLOW_DIR_ERROR
}ERROR_CODE;
typedef enum
{
	S,
	SIN
}CURVE_TYPE_;
typedef enum {
	ACC,
	DEC,
	VEL_INT_TYPE_NONE//123
}VEL_INT_TYPE;
typedef enum
{
	POS,
	PASS,
	MOTION_MOD_NONE
}MOTION_MOD;
typedef enum {
	ADV,
	BASIC
}PASS_MOD;
typedef struct VelIntrepolation
{
	double velMinRatio;
	double lenhthRatio;
	VEL_INT_TYPE velIntType;
}VelIntrepolation_;
typedef struct VelCalc
{
	double acc;
	double jerk;
	double t[4];
	double v1;
	double v2;
	double initVel;
	double endVel;
	double accTime;
	double freq;
	double timeMilli;
	double segmenPos[3];
	double decVelFlag;
	double posPre;
	double posComp;
	double initPos;
	double decPosOffset;
	int initVelEqualsEndVel;
}VelCalc_;
typedef struct VelInput
{
	double accTime;
	double smoothLevel;
	double length;
	double endVel;
	CURVE_TYPE_ curveType;
}VelInput_;
typedef struct VelOutput
{
	double vel;
	double pos;
}VelOutput_;
typedef struct VelParam
{
	VelInput_ in;
	VelCalc_  calc;
	VelOutput_ out;
	struct VelParam *next;
	struct VelParam *prev;
}VelParam;
typedef struct MovRParam
{
	int dir;
}MovRParam;
typedef struct Output
{
	double posVector[3];
	double velVector[3];
	double pos;
	double vel;
}Output;
typedef struct MovCalc
{
	double velMax;//計算最大速度(受邊界值影響)
	double length;//計算路徑長度(受二段變速影響)
	double lengthAcc;//二段變速加速度段長度	
	double lengthDec;//二段變速減速度段長度
	double velInit;//二段變速初速度
	double velEnd;//二段變速末速度
}MovCalc_;
typedef struct MovVelInput
{
	double accTime;//加速時間設定值
	double decTime;//減速時間設定值
	double velMax;//最大速度設定值
	double length;//路徑總長設定值
	double wAcc;//加速度段平滑等級設定值
	double wDec;//減速度段平滑等級設定值
	CURVE_TYPE_ curveType;//速度曲線形式設定值
	VelIntrepolation_ velInt;//二段變速參數設定值
}MovVelInput_;

typedef struct MovVelParam
{	//接收到輸入參數後，二次運算參數存到此結構
	MovCalc_  calc;//此結構計算參數
	MovVelInput_ in;//此結構輸入參數
}MovVelParam_;
typedef struct MovP_Param {
	double unitVector[3];//單位向量
	double passRatio1;//PASS模式出彎長度比例
	double passRatio2;//PASS模式入彎長度比例
	double passRatio2Pre;//上一段PASS模式入彎長度比例
	double passRatio1Pre;//上一段PASS模式出彎長度比例
	double posTurning[3];//PASS模式下出彎座標
	double velTurning[3];//PASS模式下出彎速度
	double velTarget[3];//PASS模式下入彎速度
	double posCurveTarget[3];//PASS模式下
	double velMaxPre;//上一動作之最大速度
	double B_CurveParam[3];//貝茲曲線參數
	double B_CurveTotalLength;//貝茲曲線長度
	double B_CurvePathLength;//貝茲曲線移動長度
	double B_CurveTimeCounter;//貝茲曲線移動時的計數器
	int    B_CurveCompleteFlag;
	double posStart[3];//路徑起點座標
	double posTargetPre[3];//上一段目標座標
	double posTarget[3];//路徑座標
	double posCurvePre[3];//貝茲曲線移動中，上一個cycle的目標位置
	int dir;//紀錄方向
	
	MOTION_MOD motionMod;
	PASS_MOD   passMode;
	MOTION_MOD motionModPre;
}MovP_Param_;//點到點運動功能所用到的變數

typedef struct MovVelFollow
{
	double u_1;
	double u_2;
	double y_1;
	double y_2;
	double posStart;
	double posPre;

}MovVelFollow_;
typedef struct MovOpnenFollow
{
	int dir;
	double velPre;
	double posTargetPre;
	int updateVelCurve;
	double lengthRatioPre;
	double targetVelPre;
}MovOpnenFollow_;
typedef struct MotionStopParam
{
	int dir;
	int stopInitFlag;
	
}MotionStopParam_;
typedef struct MovOPR
{
	int dir;
	int stopInitFlag;
	int dogPre;
	int initFlagDog;
	double dogOnLength;
	double squirmVel;
	double lowSpeedLength;
}MovOPR_;
typedef struct motion
{
	int initFlag;
	int interruptFlag;
	MotionStopParam_ motionStop;
	ERROR_CODE errorCode;
	MovVelParam_  movVelParam;//mov速度規畫參數
	MovOPR_ movOPR;
	MovP_Param_ movP;
	MovRParam  movR;
	MovVelFollow_ movVelFollow;
	MovOpnenFollow_ movOpenFollow;
	VelParam *velParam;//速度曲線Moudle指標變數
	Output out;
}Motion_;
void SetVelParam(Motion_ *motion, double accTime, double decTime, double velMax, double wAcc, double wDec, CURVE_TYPE_ curveType);
int MovRSingleJoint(Motion_ *motion, double feedbackPos, double TotalDis, double VelRatio);
int MovP(Motion_ *motion, double feedbackPos[3], double posTarget[3], double velRatio, MOTION_MOD motionMod);
int MovPSingleJoint(Motion_ *motion, double feedbackPos,double posTarget,double velRatio);
void SetVelIntrepolation(Motion_ *motion, double velMinRatio, double lenhthRatio, VEL_INT_TYPE velIntType);
void MotionInitial(Motion_ *motion);
void MotionChange(Motion_ *motion);
void MotionInterrupt(Motion_ *motion);
void MovPVelFollow(Motion_ *motion, double feedbackPos, double targetVel);
double GetVecLength(double v[3]);
int MovPOpenFollow(Motion_ *motion, double feedbackPos, double targetPos, double targetVel, double lowVelLimitRatio);
int MotionStop(Motion_ *motion, double StopDis);
int MovOPR(Motion_ *motion, double feedbackPos, double velRatio, double squirmVelRatio, double dogOnLength, int dog, int dir);
#endif