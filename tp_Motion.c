#include "tp_Motion.h"
//-----private function------//
static void CheckVelIntrepolationError(Motion_ *motion)
{

	if (motion->movP.motionMod == POS)
	{
		if (motion->movVelParam.in.velInt.velIntType == ACC && motion->movP.motionModPre == PASS)
		{
			motion->movVelParam.in.velInt.velIntType = VEL_INT_TYPE_NONE;

		}
	}
	else  if (motion->movP.motionMod == PASS)
	{
		motion->movVelParam.in.velInt.velIntType = VEL_INT_TYPE_NONE;
	}
}
static int ChackCollinear(double P0[3], double P1[3], double P2[3])
{
	int i;
	double AB[3], AC[3];
	double AB_Cross_AC[3];
	for (i = 0; i < 3; i++)
	{
		AB[i] = P0[i] - P1[i];
		AC[i] = P0[i] - P2[i];
	}
	AB_Cross_AC[0] = AB[1] * AC[2] - AB[2] * AC[1];
	AB_Cross_AC[1] = AB[2] * AC[0] - AB[0] * AC[2];
	AB_Cross_AC[2] = AB[0] * AC[1] - AC[0] * AB[1];
	if (fabs(AB_Cross_AC[0]) <= .0000001&&fabs(AB_Cross_AC[1]) <= .0000001&&fabs(AB_Cross_AC[2]) <= .0000001)
	{
		return 1;
	}
	else return 0;
}
static  double GetVecLength2(double u[3], double v[3])
{

	double length = 0;
	int i;

	for (i = 0; i < 3; i++)
		length += ((u[i] - v[i]) * (u[i] - v[i]));

	return length;
}
static double ChackVectorAngle(double P0[3], double P1[3])
{
	int i;
	double a = 0;
	for (i = 0; i < 3; i++)
	{
		a += (P0[i] * P1[i]);
	}
	a /= (sqrt(GetVecLength(P0))*sqrt(GetVecLength(P1)));
	return acos(a);
}
static void AddCurveWithInitVel(VelParam **node, double accTime, double smoothLevel, double length, double endVel, double initVel, CURVE_TYPE_ CURVE_TYPE)
{

	if (accTime <= 0 || smoothLevel <= 0 || length <= 0 || endVel < 0 || (CURVE_TYPE != SIN && CURVE_TYPE != S))
	{
		return;
	}
	if ((*node != NULL) && endVel == ((VelParam*)*node)->in.endVel)
	{
		((VelParam*)*node)->in.length += length;
		return;
	}
	VelParam *newVelParam = (VelParam*)malloc(sizeof(VelParam));
	//Setting new velocity parameter 
	if (fabs(endVel - initVel) <= 0.00001)
	{
		newVelParam->calc.initVelEqualsEndVel = 1;
		
	}
	else
	{
		newVelParam->calc.initVelEqualsEndVel = 0;
	
	}
	newVelParam->in.accTime = accTime;
	newVelParam->in.smoothLevel = 0.5*smoothLevel;
	newVelParam->in.length = length;
	newVelParam->in.endVel = endVel;
	newVelParam->in.curveType = CURVE_TYPE;
	newVelParam->calc.initVel = initVel;
	newVelParam->next = NULL;
	newVelParam->prev = NULL;
	//Createe first node
	if (*node == NULL) {
		*node = newVelParam;
		return;
	}
	else {
		//Set node
		VelParam *current;
		current = *node;
		current->next = newVelParam;
		*node = current->next;
		newVelParam->prev = current;
		return;
	}
}
static void AddCurve(VelParam **node, double accTime, double smoothLevel, double length, double endVel, CURVE_TYPE_ CURVE_TYPE)
{


	if (accTime <= 0 || smoothLevel <= 0 || smoothLevel >= 1 || length <= 0 || endVel < 0 || (CURVE_TYPE != SIN && CURVE_TYPE != S))
	{
		return;
	}
	if ((*node != NULL) && endVel == ((VelParam*)*node)->in.endVel)
	{
		((VelParam*)*node)->in.length += length;
		return;
	}
	VelParam *newVelParam = (VelParam*)malloc(sizeof(VelParam));
	//Setting new velocity parameter 
	newVelParam->calc.initVelEqualsEndVel = 0;
	newVelParam->in.accTime = accTime;
	newVelParam->in.smoothLevel = 0.5*smoothLevel;
	newVelParam->in.length = length;
	newVelParam->in.endVel = endVel;
	newVelParam->in.curveType = CURVE_TYPE;
	newVelParam->calc.initVel = 0;
	newVelParam->next = NULL;
	newVelParam->prev = NULL;
	//Createe first node
	if (*node == NULL) {
		*node = newVelParam;
		return;
	}
	else {
		//Set node
		VelParam *current;
		current = *node;
		current->next = newVelParam;
		*node = current->next;
		newVelParam->prev = current;
		return;
	}
}
static void VelParamCalc(VelParam *V)
{
	if (V->prev != NULL)
	{
		if (V->calc.initVelEqualsEndVel)
		{
			V->calc.t[0] = V->in.length / V->in.endVel;
			V->calc.timeMilli = 0;
			V->calc.endVel = V->in.endVel;
		}
		else
		{
			V->calc.initVel = V->prev->in.endVel;
			if (V->calc.initVel > V->in.endVel)
			{
				double initValueBuffer = V->calc.initVel;
				V->calc.initVel = V->in.endVel;
				V->calc.endVel = initValueBuffer;
				V->calc.decVelFlag = -1;
				V->calc.decPosOffset = V->in.length;
			}
			else
			{
				V->calc.decPosOffset = 0;
				V->calc.endVel = V->in.endVel;
				V->calc.decVelFlag = 1;
			}
		}

		
		V->calc.initPos = V->prev->out.pos;
		V->calc.posPre = V->calc.initPos;

	}
	else if (V->prev == NULL && V->calc.initVel != 0)
	{
		 if (V->calc.initVel > V->in.endVel&&!V->calc.initVelEqualsEndVel)
		{
			double initValueBuffer = V->calc.initVel;
			V->calc.initVel = V->in.endVel;
			V->calc.endVel = initValueBuffer;
			V->calc.decVelFlag = -1;
			V->calc.decPosOffset = V->in.length;
		}
		else
		{
			 if (V->calc.initVelEqualsEndVel)
			 {
				 V->calc.t[0] = V->in.length / V->in.endVel;
				 V->calc.timeMilli = 0;
			 }
			V->calc.decPosOffset = 0;
			V->calc.endVel = V->in.endVel;
			V->calc.decVelFlag = 1;
		}
		V->calc.initPos = 0;
		V->calc.posPre = 0;
	}
	else
	{
		V->calc.endVel = V->in.endVel;
		V->calc.decVelFlag = 1;
		V->calc.decPosOffset = 0;
		V->calc.initPos = 0;
		V->calc.posPre = 0;
	}
	//checking if acctime more then the boundary
	double boundary;
	double length1, length2;
	boundary = (0.5*V->in.accTime*V->calc.endVel + V->calc.initVel *V->in.accTime) / V->in.length;
	if (boundary >= 1)
	{
		V->calc.accTime = V->in.length / (0.5*V->calc.endVel + V->calc.initVel);
	}
	else
	{
		V->calc.accTime = V->in.accTime;
	}
	length1 = 0.5*V->calc.accTime*V->calc.endVel + V->calc.initVel  * V->calc.accTime;
	length2 = V->in.length - length1;
	if (V->in.curveType == S)
	{
		V->calc.t[0] = 4 * V->in.smoothLevel*length1  \
			/ ((V->calc.endVel + V->calc.initVel)*(2 * V->in.smoothLevel + 1));
		V->calc.jerk = 2 * V->in.smoothLevel * (V->calc.endVel - V->calc.initVel) / V->calc.t[0] / V->calc.t[0];
		V->calc.acc = V->calc.jerk  * V->calc.t[0];
		V->calc.v1 = V->in.smoothLevel * (V->calc.endVel - V->calc.initVel) + V->calc.initVel;
		V->calc.v2 = (1 - V->in.smoothLevel)* (V->calc.endVel - V->calc.initVel) + V->calc.initVel;
		V->calc.t[1] = (V->calc.v2 - V->calc.v1) / V->calc.acc + V->calc.t[0];
		V->calc.t[2] = V->calc.t[1] + V->calc.t[0];
		V->calc.t[3] = V->calc.t[2] + length2 / V->calc.endVel;
		V->calc.segmenPos[0] = V->calc.jerk * V->calc.t[0] * V->calc.t[0] * V->calc.t[0] / 6 + V->calc.initVel *V->calc.t[0];
		V->calc.segmenPos[1] = 0.5*V->calc.acc*(V->calc.t[1] - V->calc.t[0])*(V->calc.t[1] - V->calc.t[0]) + V->calc.v1 * (V->calc.t[1] - V->calc.t[0]) + V->calc.segmenPos[0];
		V->calc.segmenPos[2] = length1;
	}
	else if (V->in.curveType == SIN)
	{
		V->calc.t[0] = (V->in.smoothLevel*PI*length1) \
			/ ((V->calc.endVel + V->calc.initVel)) \
			/ (V->in.smoothLevel*PI - 2 * V->in.smoothLevel + 1);
		V->calc.acc = V->in.smoothLevel * (V->calc.endVel - V->calc.initVel)*PI / 2 / V->calc.t[0];
		V->calc.freq = PI / 2 / V->calc.t[0];
		V->calc.v1 = V->in.smoothLevel* (V->calc.endVel - V->calc.initVel) + V->calc.initVel;
		V->calc.v2 = (1 - V->in.smoothLevel)*(V->calc.endVel - V->calc.initVel) + V->calc.initVel;
		V->calc.t[1] = (V->calc.v2 - V->calc.v1) / V->calc.acc + V->calc.t[0];
		V->calc.t[2] = V->calc.t[1] + V->calc.t[0];
		V->calc.t[3] = V->calc.t[2] + length2 / V->calc.endVel;
		V->calc.segmenPos[0] = -V->calc.acc / V->calc.freq * (-V->calc.t[0] + sin(V->calc.freq *V->calc.t[0]) / V->calc.freq) + V->calc.initVel*V->calc.t[0];
		V->calc.segmenPos[1] = 0.5*V->calc.acc*(V->calc.t[1] - V->calc.t[0])*(V->calc.t[1] - V->calc.t[0]) + V->calc.v1 * (V->calc.t[1] - V->calc.t[0]) + V->calc.segmenPos[0];
		V->calc.segmenPos[2] = length1;
	}
	if (V->calc.decVelFlag == -1)
	{
		//V->calc.timeMilli = (double)((int)((V->calc.t[3] - 1) * 10000)) / 10000;

		V->calc.timeMilli = (V->calc.t[3] - 1);

	}
	else
	{
		V->calc.timeMilli = 1;
	}
	V->calc.posComp = 0;
}
static int VelOutput(VelParam **node)
{
	while (1)
	{
		VelParam *V;
		V = *node;
		double t = V->calc.timeMilli;
		double pos = 0;
		if (!V->calc.initVelEqualsEndVel)
		{
			if (V->in.curveType == S) {
				if (t < 0)
				{
					pos = 0;
					V->out.pos = (V->calc.decPosOffset + (V->calc.decVelFlag)*pos) + V->calc.initPos;
					if (V->next != NULL) {
						V->next->in.length += (V->calc.posComp);
						VelParamCalc(V->next);
						*node = V->next;
						continue;
					}
					else
					{
						V->out.vel = 0;
						return 1;
					}
				}
				if (t >= 0 && t <= V->calc.t[0])
				{
					pos = V->calc.jerk* t*t*t / 6 + V->calc.initVel*t;
				}
				else if (t <= V->calc.t[1] && t > V->calc.t[0])
				{
					pos = 0.5*V->calc.acc*(t - V->calc.t[0])*(t - V->calc.t[0]) + V->calc.v1 * (t - V->calc.t[0]) + V->calc.segmenPos[0];
				}
				else if (t <= V->calc.t[2] && t > V->calc.t[1])
				{

					pos = 0.5*V->calc.acc*(t - V->calc.t[1])*(t - V->calc.t[1]) + V->calc.v2 * (t - V->calc.t[1]) - V->calc.jerk * (t - V->calc.t[1])*(t - V->calc.t[1])*(t - V->calc.t[1]) / 6 + V->calc.segmenPos[1];

				}
				else if (t <= V->calc.t[3] && t > V->calc.t[2])
				{
					pos = V->calc.endVel * (t - V->calc.t[2]) + V->calc.segmenPos[2];

				}
				else if (t > V->calc.t[3])
				{
					pos = V->in.length;
					if (V->next != NULL)
					{
						V->next->in.length += (V->in.length - V->calc.posComp);
						VelParamCalc(V->next);
						*node = V->next;
						continue;
					}
					else
					{
						V->out.pos = (V->calc.decPosOffset + (V->calc.decVelFlag)*pos) + V->calc.initPos;
						V->out.vel = (V->out.pos) - V->calc.posPre;
						V->calc.posPre = V->out.pos;
						return 1;
					}
				}
			}
			else if (V->in.curveType == SIN) {
				if (t < 0)
				{
					pos = 0;

					if (V->next != NULL) {
						V->next->in.length += (V->calc.posComp);
						VelParamCalc(V->next);
						*node = V->next;
						continue;

					}
					else
					{
						V->out.vel = 0;
						return 1;
					}
				}
				if (t >= 0 && t <= V->calc.t[0])
				{	
					pos = -V->calc.acc / V->calc.freq * (-t + sin(V->calc.freq*t) / V->calc.freq) + V->calc.initVel*(t);
				}
				else if (t > V->calc.t[0] && t <= V->calc.t[1])
				{

					pos = 0.5*V->calc.acc*(t - V->calc.t[0])*(t - V->calc.t[0]) + V->calc.v1 * (t - V->calc.t[0]) + V->calc.segmenPos[0];
				}
				else if (t > V->calc.t[1] && t <= V->calc.t[2])
				{
					pos = V->calc.v2 * (t - V->calc.t[1]) - (V->calc.acc / V->calc.freq / V->calc.freq)*(cos(V->calc.freq*(t - V->calc.t[1])) - 1) + V->calc.segmenPos[1];


				}
				else if (t > V->calc.t[2] && t <= V->calc.t[3])
				{
					pos = V->calc.segmenPos[2] + V->calc.endVel * (t - V->calc.t[2]);
				}
				else if (t > V->calc.t[3])
				{
					pos = V->in.length;
					if (V->next != NULL)
					{
						V->next->in.length += (V->in.length - V->calc.posComp);
						VelParamCalc(V->next);
						*node = V->next;
						continue;
					}
					else
					{
						V->out.pos = (V->calc.decPosOffset + (V->calc.decVelFlag)*pos) + V->calc.initPos;
						V->out.vel = (V->out.pos) - V->calc.posPre;
						V->calc.posPre = V->out.pos;
						return 1;
					}
				}
			}
			if (V->calc.decVelFlag == -1)
			{
				V->calc.timeMilli--;
			}
			else
			{
				V->calc.timeMilli++;
			}
		}
		else
		{
			if (t >= 0 && t <= V->calc.t[0])
			{
				pos = V->calc.endVel*t ;
			}
			else if (t > V->calc.t[0])
			{
				pos = V->in.length;
				if (V->next != NULL)
				{
					V->next->in.length += (V->in.length - V->calc.posComp);
					VelParamCalc(V->next);
					*node = V->next;
					continue;
				}
				else
				{
					V->out.vel = 0;
					return 1;
				}
			}
			V->calc.timeMilli++;
		}
		V->out.pos = (V->calc.decPosOffset + (V->calc.decVelFlag)*pos) + V->calc.initPos;
		V->out.vel = (V->out.pos) - V->calc.posPre;
		V->calc.posPre = V->out.pos;
		V->calc.posComp = pos;
		return 0;
	}
}
static void EndCurve(VelParam **node)
{
	VelParam * V;
	V = *node;
	if (V == NULL)
	{
		return;
	}
	while (V->prev != NULL)
	{
		*node = V->prev;
		V = *node;
	}
	VelParamCalc(V);

}
static void StartCurve(VelParam **node)
{
	*node = NULL;
}
static double LengthToVel(Motion_ *motion, double length)
{
	MovCalc_ *calc = &motion->movVelParam.calc;
	MovVelInput_ *in = &motion->movVelParam.in;
	double time = 0;
	double triangleArea = 0.5*(in->accTime*calc->velMax);
	double vel;
	int tempFlag;
	if (length <= 0.5*in->length)
	{
		if (length < triangleArea)
		{
			vel = sqrt(2 * (calc->velMax / in->accTime)*length);
		}
		else
		{
			vel = calc->velMax;
		}

	}
	else if (length > 0.5*in->length)
	{
		if (length > (in->length - triangleArea))
		{
			if (in->length - length >= 0)
			{
				vel = sqrt(2 * (calc->velMax / in->decTime)*(in->length - length));
			}
			else
			{
				vel = 0;
				length = in->length;
			}
		}
		else
		{
			vel = calc->velMax;
		}


	}
	return vel;
}
static int CreatePassVelCurve(Motion_  *motion, double length, double initVel, double initLength)
{
	MovCalc_ *calc = &motion->movVelParam.calc;
	MovVelInput_ *in = &motion->movVelParam.in;
	double time = 0;
	double triangleArea = 0.5*(in->accTime*calc->velMax);
	double vel;
	int tempFlag;
	if (motion->movVelParam.in.velInt.velIntType == VEL_INT_TYPE_NONE)
	{
		vel = LengthToVel(motion, length);
	}
	else
	{
		vel = motion->movVelParam.calc.velEnd;
	}

	if (length <= 0.5*in->length)
	{
		StartCurve(&motion->velParam);
		if (initVel == 0 || initLength == 0)
		{
			AddCurve(&motion->velParam, in->accTime, in->wAcc, length, vel, in->curveType);
			AddCurve(&motion->velParam, in->decTime, in->wDec, in->length - length, 0, in->curveType);
		}
		else
		{
			if (length - initLength <= 0)
			{
				return 1;
			}
			AddCurveWithInitVel(&motion->velParam, in->accTime, in->wAcc, length - initLength, vel, initVel, in->curveType);
			AddCurve(&motion->velParam, in->decTime, in->wDec, in->length - length, 0, in->curveType);
		}
		EndCurve(&motion->velParam);
		return 0;
	}
	else if (length > 0.5*in->length)
	{
		StartCurve(&motion->velParam);
		if (initVel == 0 || initLength == 0)
		{
			AddCurve(&motion->velParam, in->accTime, in->wAcc, 0.5*in->length, calc->velMax, in->curveType);
			AddCurve(&motion->velParam, in->decTime, in->wDec, length - 0.5*in->length, vel, in->curveType);
			AddCurve(&motion->velParam, in->decTime, in->wDec, in->length - length, 0, in->curveType);
		}
		else
		{
			if (length - initLength <= 0)
			{
				return 1;
			}
			if (initLength < 0.5*in->length)
			{
				AddCurveWithInitVel(&motion->velParam, in->accTime, in->wAcc, 0.5*in->length - initLength, calc->velMax, initVel, in->curveType);
				AddCurve(&motion->velParam, in->decTime, in->wDec, length - 0.5*in->length, vel, in->curveType);
				AddCurve(&motion->velParam, in->decTime, in->wDec, in->length - length, 0, in->curveType);
			}
			else
			{
				AddCurveWithInitVel(&motion->velParam, in->accTime, in->wAcc, length - initLength, vel, initVel, in->curveType);
				AddCurve(&motion->velParam, in->decTime, in->wDec, in->length - length, 0, in->curveType);
			}
		}
		EndCurve(&motion->velParam);
		return 0;
	}
}
static void CreateBezerVelCurve(Motion_  *motion, double maxVel, double velTarget)
{
	double initVel = sqrt(GetVecLength(motion->movP.velTurning));
	double length = motion->movP.B_CurveTotalLength;
	MovCalc_ *calc = &motion->movVelParam.calc;
	MovVelInput_ *in = &motion->movVelParam.in;
	StartCurve(&motion->velParam);
	AddCurveWithInitVel(&motion->velParam, in->accTime, in->wAcc, 0.5*length, maxVel, initVel, in->curveType);
	AddCurve(&motion->velParam, in->decTime, in->wDec, 0.5*length, velTarget, in->curveType);
	AddCurve(&motion->velParam, in->decTime, in->wDec, (1 - motion->movP.passRatio2Pre)*calc->length, 0, in->curveType);
	EndCurve(&motion->velParam);

}
static void MovParamCalc(MovVelParam_ *p)
{

	double boundAcc;
	double boundDec;
	double VelRatio;
	if (p->in.velInt.velIntType == ACC)
	{
		p->calc.lengthAcc = (p->in.velInt.lenhthRatio* p->in.length);
		p->calc.lengthDec = 0;
		p->calc.length = p->in.length - p->calc.lengthAcc;
		p->calc.velInit = p->in.velInt.velMinRatio*p->calc.velMax;
		p->calc.velEnd = 0;
	}
	else if (p->in.velInt.velIntType == DEC)
	{
		p->calc.lengthDec = ((1 - p->in.velInt.lenhthRatio)* p->in.length);
		p->calc.lengthAcc = 0;
		p->calc.length = p->in.length - p->calc.lengthDec;
		p->calc.velInit = 0;
		p->calc.velEnd = p->in.velInt.velMinRatio*p->calc.velMax;
	}
	else
	{
		p->calc.lengthAcc = 0;
		p->calc.lengthDec = 0;
		p->calc.velEnd = 0;
		p->calc.velInit = 0;
	}
	boundAcc = ((p->calc.velMax + p->calc.velInit)*p->in.accTime) / p->in.length;
	boundDec = ((p->calc.velMax + p->calc.velEnd)*p->in.decTime) / p->in.length;
	if (boundAcc > 1 || boundDec > 1)
	{
		if (boundAcc > boundDec)
		{
			VelRatio = (p->calc.length / p->in.accTime) / (p->calc.velMax + p->calc.velInit);
			p->calc.velMax = p->calc.velMax*VelRatio;
			p->calc.velEnd *= VelRatio;
			p->calc.velInit *= VelRatio;
		}
		else
		{
			VelRatio = (p->calc.length / p->in.decTime) / (p->calc.velMax + p->calc.velEnd);
			p->calc.velMax = p->calc.velMax*VelRatio;
			p->calc.velInit *= VelRatio;
			p->calc.velEnd *= VelRatio;
		}
	}
}
static void FreeCurvList(VelParam **node)
{
	VelParam *V;
	V = *node;

	while (V != NULL) {
		VelParam *current;
		current = V;
		V = V->prev;
		free(current);
	}
	*node = NULL;
}
static void CreateVelCurve(Motion_ *motion)
{
	MovVelInput_ *in = &motion->movVelParam.in;
	MovCalc_ *calc = &motion->movVelParam.calc;
	//速度參數進行邊界值運算
	MovParamCalc(&motion->movVelParam);
	//建立速度規畫曲線
	StartCurve(&motion->velParam);
	AddCurve(&motion->velParam, in->accTime, in->wAcc, calc->lengthAcc, calc->velInit, in->curveType);
	AddCurve(&motion->velParam, in->accTime, in->wAcc,fabs(calc->length / 2), calc->velMax, in->curveType);
	AddCurve(&motion->velParam, in->decTime, in->wDec, fabs(calc->length / 2), calc->velEnd, in->curveType);
	AddCurve(&motion->velParam, in->decTime, in->wDec, fabs( calc->lengthDec), 0, in->curveType);
	EndCurve(&motion->velParam);
}
static double B_l(double t, double A, double B, double C)
{
	//printf("%lf,%lf,%lf\n", A, B, C);
	double temp1 = sqrt(C + t * (B + A * t));
	double temp2 = (2 * A * t * temp1 + B * (temp1 - sqrt(C)));
	double temp3 = log(B + 2 * sqrt(A) * sqrt(C));
	double temp4 = log(B + 2 * A * t + 2 * sqrt(A) * temp1);
	double temp5 = 2 * sqrt(A) * temp2;
	double temp6 = (B * B - 4 * A * C) * (temp3 - temp4);
	double temp7;
	temp7 = (temp5 + temp6) / (8 * pow(A, 1.5));
	//printf("%lf\n", temp7);
	return temp7;
}
static int BezerCurveVelOutput(Motion_ *motion)
{
	int i, counter = 0, motio;
	double s, t1 = 0, t2 = 0;
	double P_Curve[3], CurvVolacity[3];
	double err;
	MovP_Param_ *movP;
	movP = &motion->movP;

	if (movP->B_CurveTimeCounter <= 1)
	{
		movP->B_CurveCompleteFlag = 0;
		VelOutput(&motion->velParam);
		movP->B_CurvePathLength = motion->velParam->out.pos;
		while (1)
		{
			s = sqrt(movP->B_CurveParam[0] * t1 * t1 + movP->B_CurveParam[1] * t1 + movP->B_CurveParam[2]);
			t2 = t1 - (B_l(t1, movP->B_CurveParam[0], movP->B_CurveParam[1], movP->B_CurveParam[2]) - movP->B_CurvePathLength) / s;
			if (fabs(t1 - t2) < 0.0001)
				break;
			t1 = t2;
			if (counter >= 10)
			{
				counter = 0;
				break;
			}
			counter++;
		}

		for (i = 0; i < 3; i++)
		{
			P_Curve[i] = (1 - t2) * (1 - t2) *movP->posTurning[i] + 2 * t2 * (1 - t2) *movP->posTargetPre[i] + t2 * t2 * movP->posCurveTarget[i];
			CurvVolacity[i] = (P_Curve[i] - motion->out.posVector[i]);
		}
		err = motion->velParam->out.vel - sqrt(GetVecLength(CurvVolacity));
		for (i = 0; i < 3; i++)
		{
			motion->out.posVector[i] = P_Curve[i] + err * CurvVolacity[i] / sqrt(GetVecLength(CurvVolacity));
			motion->out.velVector[i] = motion->out.posVector[i] - movP->posCurvePre[i];
			movP->posCurvePre[i] = motion->out.posVector[i];
		}
		movP->B_CurveTimeCounter = t2;
		return 0;
	}
	else
	{
		double temp, temp1, temp2, a = 0, b = 0, c = 0, compLength;
		double P1[3], P2[3];
		double vecCheck1[3], vecCheck2[3], vecAngle;
		if (!ChackCollinear(motion->out.posVector, movP->posTargetPre, movP->posTarget))
		{
			for (i = 0; i < 3; i++)
			{
				temp = motion->out.posVector[i] - movP->posTargetPre[i];
				c += temp * temp;
				b += -2 * (temp * movP->unitVector[i]);
				a += movP->unitVector[i] * movP->unitVector[i];
			}
			compLength = sqrt(GetVecLength(motion->out.velVector));
			c -= compLength * compLength;
			temp1 = (-b + sqrt(b*b - 4 * a*c)) / 2 / a;
			temp2 = (-b - sqrt(b*b - 4 * a*c)) / 2 / a;
			for (i = 0; i < 3; i++)
			{
				P1[i] = temp1 * movP->unitVector[i] + movP->posTargetPre[i];
				P2[i] = temp2 * movP->unitVector[i] + movP->posTargetPre[i];
				vecCheck1[i] = P1[i] - motion->out.posVector[i];
				vecCheck2[i] = P2[i] - motion->out.posVector[i];
			}
			if (ChackVectorAngle(vecCheck1, movP->unitVector) < ChackVectorAngle(vecCheck2, movP->unitVector))
			{
				for (i = 0; i < 3; i++)
				{
					motion->out.posVector[i] = P1[i];
				}
			}
			else
			{
				for (i = 0; i < 3; i++)
				{
					motion->out.posVector[i] = P2[i];
				}
			}
			double targetCurveVel = sqrt(GetVecLength(motion->out.velVector));
			double  len;
			if (movP->motionMod == POS)
			{
				len = motion->movVelParam.calc.length;
			}
			else
			{
				if (movP->passRatio2Pre >= movP->passRatio1)
				{
					len = motion->movVelParam.in.length*movP->passRatio2Pre;
				}
				else
				{
					len = movP->passRatio1*motion->movVelParam.in.length;
				}
			}
			double temp = (sqrt(GetVecLength2(motion->out.posVector, movP->posTarget)));
			FreeCurvList(&motion->velParam);
			if (!CreatePassVelCurve(motion, len, targetCurveVel, motion->movVelParam.in.length - temp))
			{
				return 0;
			}
			else
			{
				return 99;//上一段入彎長度大於目前動作出彎長度，直接進行曲線運動
			}

		}
		return 1;
	}
}
static int PassModInit(Motion_ *motion)
{
	int i = 0;
	double temp1[3], temp2[3];
	MovP_Param_ *movP;
	movP = &motion->movP;
	MovCalc_ *calc = &motion->movVelParam.calc;
	MovVelInput_ *in = &motion->movVelParam.in;

	for (i = 0; i < 3; i++)
	{
		temp1[i] = movP->posTurning[i] - 2 * movP->posTargetPre[i] + movP->posCurveTarget[i];
		temp2[i] = 2 * movP->posTargetPre[i] - 2 * movP->posTurning[i];
	}
	movP->B_CurveParam[0] = 4 * (temp1[0] * temp1[0] + temp1[1] * temp1[1] + temp1[2] * temp1[2]);
	movP->B_CurveParam[1] = 4 * (temp1[0] * temp2[0] + temp1[1] * temp2[1] + temp1[2] * temp2[2]);
	movP->B_CurveParam[2] = temp2[0] * temp2[0] + temp2[1] * temp2[1] + temp2[2] * temp2[2];
	movP->B_CurveTotalLength = B_l(1, movP->B_CurveParam[0], movP->B_CurveParam[1], movP->B_CurveParam[2]);
	double maxVel, velTarget;
	velTarget = LengthToVel(motion, in->length*(1 - movP->passRatio2Pre));
	MovParamCalc(&motion->movVelParam);
	if (!(movP->passRatio2Pre <= 0.5&&movP->passRatio1Pre >= 0.5))
	{
		if (movP->velMaxPre >= motion->movVelParam.calc.velMax)
		{
			maxVel = movP->velMaxPre;
		}
		else
		{
			maxVel = motion->movVelParam.calc.velMax;
		}
	}
	else
	{
		maxVel = velTarget;
	}
	CreateBezerVelCurve(motion, maxVel, velTarget);
	movP->B_CurveTimeCounter = 0;
}
static void CreateStopVelCurve(Motion_ *motion, double initVel, double length)
{
	MovCalc_ *calc = &motion->movVelParam.calc;
	MovVelInput_ *in = &motion->movVelParam.in;
	StartCurve(&motion->velParam);
	AddCurveWithInitVel(&motion->velParam, in->decTime, in->wDec, length, 0, initVel, in->curveType);
	EndCurve(&motion->velParam);
}
static double SD(double num1, double num2, double num3)
{
	double u = (num1 + num2 + num3) / 3;

	double temp, temp1[3];
	int i;
	temp = (num1 - u)*(num1 - u) + (num2 - u)*(num2 - u) + (num3 - u)*(num3 - u);
	return sqrt(temp / 3);
}
static void SetDynamicInputParam(Motion_ *motion, double totalDis, double velRatio)
{
	MovCalc_ *calc = &motion->movVelParam.calc;
	MovVelInput_ *in = &motion->movVelParam.in;
	in->length = totalDis;
	calc->velMax = in->velMax* velRatio;
	calc->length = in->length;
}
//-----public function------//
void SetVelIntrepolation(Motion_ *motion, double velMinRatio, double lenhthRatio, VEL_INT_TYPE velIntType)
{

	if (velMinRatio <= 0 || lenhthRatio <= 0 || velMinRatio >= 1 || lenhthRatio >= 1 || velIntType == VEL_INT_TYPE_NONE)
	{
		motion->movVelParam.in.velInt.velIntType = VEL_INT_TYPE_NONE;
		return;
	}
	motion->movVelParam.in.velInt.velMinRatio = velMinRatio;
	motion->movVelParam.in.velInt.lenhthRatio = lenhthRatio;
	motion->movVelParam.in.velInt.velIntType = velIntType;

};
double GetVecLength(double v[3])
{
	double length;
	length = v[0] * v[0] + v[1] * v[1] + v[2] * v[2];

	return length;
}
void MotionChange(Motion_ *motion)
{

	SetVelIntrepolation(motion, 0, 0, VEL_INT_TYPE_NONE);
	motion->initFlag = 0;
	motion->motionStop.stopInitFlag = 0;
	//FreeCurvList(&motion->velParam);
}
void MotionInterrupt(Motion_ *motion)
{
	int i;
	FreeCurvList(&motion->velParam);
	motion->interruptFlag = 1;
	motion->out.pos = 0;
	motion->out.vel = 0;
	for (i = 0; i < 3; i++)
	{
		motion->out.posVector[i] = 0;
		motion->out.velVector[i] = 0;
	}
}
void SetPassLengthRatio_A(Motion_ *motion, double  passRatio1, double  passRatio2)
{
	motion->movP.passMode = ADV;
	motion->movP.passRatio1 = passRatio1;
	motion->movP.passRatio2 = passRatio2;
	if (motion->movP.passRatio1 >= 0.95)
	{
		motion->movP.passRatio1 = 0.95;
	}
	else if (motion->movP.passRatio1 <= 0.05)
	{
		motion->movP.passRatio1 = 0.05;
	}
	if (motion->movP.passRatio2 >= 0.95)
	{
		motion->movP.passRatio2 = 0.95;
	}
	else if (motion->movP.passRatio2 <= 0.05)
	{
		motion->movP.passRatio2 = 0.05;
	}

}
void MotionInitial(Motion_ *motion)
{
	int i;
	motion->interruptFlag = 0;
	motion->initFlag = 0;
	motion->movVelParam.in.velInt.velIntType = VEL_INT_TYPE_NONE;
	motion->out.pos = 0;
	motion->out.vel = 0;
	motion->movP.motionModPre = POS;
	//FreeCurvList(&motion->velParam);
	motion->velParam = NULL;
	motion->motionStop.stopInitFlag = 0;
	for (i = 0; i < 3; i++)
	{
		motion->out.posVector[i] = 0;
		motion->out.velVector[i] = 0;
	}
}
int MovP(Motion_ *motion, double feedbackPos[3], double posTarget[3], double velRatio, MOTION_MOD motionMod)
{	int i;
	if (!motion->interruptFlag)
	{
		MovP_Param_ *movP;
		movP = &motion->movP;
		MovCalc_ *calc = &motion->movVelParam.calc;
		MovVelInput_ *in = &motion->movVelParam.in;
		
		double totalLength;
		int motionFinish;
		if (motion->errorCode == VEL_PARAM_ERROR)
		{
			return 0;
		}
		movP->motionMod = motionMod;
		CheckVelIntrepolationError(motion);
	
		if (motion->initFlag == 0)
		{
			if (velRatio < 0)
			{
				return 0;
			}
			else if (velRatio >= 1)
			{
				velRatio = 1;
			}
			double lenghtRatio = 1;
			
			if (movP->motionModPre == PASS)
			{
				if (movP->motionMod == POS && in->velInt.velIntType == DEC)
				{
					lenghtRatio = motion->movVelParam.in.velInt.lenhthRatio;
				}
				movP->B_CurveTimeCounter = 0;
				SetDynamicInputParam(motion, sqrt(GetVecLength2(movP->posTargetPre, posTarget)), velRatio);
				for (i = 0; i < 3; i++)
				{
					movP->unitVector[i] = (posTarget[i] - movP->posTargetPre[i]) / in->length;
					movP->posStart[i] = movP->posTargetPre[i];
					movP->posTarget[i] = posTarget[i];
					movP->posCurveTarget[i] = movP->posTargetPre[i] + lenghtRatio * in->length*movP->passRatio2Pre *movP->unitVector[i];
					movP->posCurvePre[i] = movP->posTurning[i];
				}
				if (in->length <= 0.0001 || ChackCollinear(movP->posTurning, movP->posTargetPre, movP->posCurveTarget))
				{
					//input same point or input collinear point
					movP->motionModPre = POS;
					movP->motionMod = POS;
					CreateStopVelCurve(motion, sqrt(GetVecLength(motion->movP.velTurning)), sqrt(GetVecLength2(movP->posTurning, posTarget)));
					for (i = 0; i < 3; i++)
					{
						movP->unitVector[i] = (posTarget[i] - movP->posTurning[i]) / sqrt(GetVecLength2(movP->posTurning, posTarget));
						movP->posStart[i] = movP->posTurning[i];
						movP->posTarget[i] = posTarget[i];
					}
					motion->errorCode = PASS_INPUT_POINT_ERROR;
				}
				else
				{
					PassModInit(motion);
				}
				movP->B_CurveCompleteFlag = 0;
			}
			else if (movP->motionModPre == POS)
			{
				movP->B_CurveTimeCounter = 0;
				totalLength = sqrt(GetVecLength2(feedbackPos, posTarget));
				//input same point
				if (totalLength <= 0.0001)
				{
					motion->errorCode = POS_INPUT_POINT_ERROR;
					return 0;
				}
				SetDynamicInputParam(motion, totalLength, velRatio);
				for (i = 0; i < 3; i++)
				{
					movP->unitVector[i] = (posTarget[i] - feedbackPos[i]) / totalLength;
					movP->posStart[i] = feedbackPos[i];
					movP->posTarget[i] = posTarget[i];
					motion->out.posVector[i] = movP->posStart[i];
				}
				if (movP->motionMod == POS)
				{
					CreateVelCurve(motion);
					movP->velMaxPre = motion->movVelParam.calc.velMax;
				}
				else if (movP->motionMod == PASS)
				{
					MovParamCalc(&motion->movVelParam);
					movP->velMaxPre = motion->movVelParam.calc.velMax;
					CreatePassVelCurve(motion, in->length*movP->passRatio1, 0, 0);
				}
				movP->B_CurveCompleteFlag = 1;
			}
			motion->initFlag = 1;
			motion->errorCode = NO_ERROR;
		}
		if (motion->initFlag == 1)
		{
			if (movP->motionModPre == PASS && movP->B_CurveCompleteFlag == 0)
			{
				//move corner curve
				if (movP->B_CurveCompleteFlag == 0)
				{
					movP->B_CurveCompleteFlag = BezerCurveVelOutput(motion);
					return 0;
				}
			}
			else if  (movP->B_CurveCompleteFlag != 99)
			{
				motionFinish = VelOutput(&motion->velParam);
				for (i = 0; i < 3; i++)
				{
					motion->out.posVector[i] += motion->velParam->out.vel*movP->unitVector[i];
					motion->out.velVector[i] = motion->velParam->out.vel*movP->unitVector[i];
				}
			}
			if ((movP->motionMod == PASS && sqrt(GetVecLength2(motion->out.posVector, movP->posStart)) >= motion->movVelParam.in.length * movP->passRatio1) || (movP->B_CurveCompleteFlag == 99))
			{
				motion->movP.motionModPre = motion->movP.motionMod;
				movP->passRatio2Pre = movP->passRatio2;
				movP->passRatio1Pre = movP->passRatio1;
				for (i = 0; i < 3; i++)
				{
					movP->posTurning[i] = motion->out.posVector[i];
					movP->velTurning[i] = motion->out.velVector[i];
					movP->posTargetPre[i] = posTarget[i];
				}
				return 1;
			}
			else
			{
				if (motionFinish == 1)
				{
					motion->movP.motionModPre = motion->movP.motionMod;
				}
				return motionFinish;
			}
		}
	}
	for (i = 0; i < 3; i++)
	{
		motion->out.posVector[i] = feedbackPos[i];
		motion->out.velVector[i] = 0;
	}

	return 0;

}
int MovRSingleJoint(Motion_ *motion, double feedbackPos, double TotalDis, double velRatio)
{
	if (!motion->interruptFlag&!motion->motionStop.stopInitFlag)
	{
		int i;
		if (motion->initFlag == 0)
		{
			if (motion->errorCode == VEL_PARAM_ERROR)
			{
				return 0;
			}
			FreeCurvList(&motion->velParam);
			//紀錄方向
			if (TotalDis < 0)
			{
				motion->movR.dir = -1;
			}
			else if (TotalDis > 0)
			{
				motion->movR.dir = 1;
			}
			else
			{
				return 0;

			}
			//建立速度曲線
			SetDynamicInputParam(motion, fabs(TotalDis), velRatio);
			CreateVelCurve(motion);
			motion->out.pos = feedbackPos;
			motion->out.vel = 0.0;
			//初始化完畢
			motion->initFlag = 1;
		}
		if (motion->initFlag == 1)
		{
			int motionFinish;
			motionFinish = VelOutput(&motion->velParam);
			motion->out.pos+= motion->movR.dir*motion->velParam->out.vel;
			motion->out.vel = motion->movR.dir*motion->velParam->out.vel;
			for (i = 0; i < 3; i++)
			{
				motion->out.posVector[i] = 0;
				motion->out.velVector[i] = 0;
			}
			return motionFinish;
		}
	}
	motion->out.pos = feedbackPos;
	motion->out.vel = 0;
	return 0;
}
void SetVelParam(Motion_ *motion, double accTime, double decTime, double velMax, double wAcc, double wDec, CURVE_TYPE_ curveType)
{
	motion->movVelParam.in.accTime = accTime;
	motion->movVelParam.in.decTime = decTime;
	motion->movVelParam.in.velMax = velMax;
	motion->movVelParam.in.wAcc = wAcc;
	motion->movVelParam.in.wDec = wDec;
	motion->movVelParam.in.curveType = curveType;
	if (wAcc >= 1)
	{
		wAcc = 0.99;
	}
	if (wDec >= 1)
	{
		wAcc = 0.99;
	}
	if (accTime <= 0 || decTime <= 0 || velMax <= 0 || wAcc <= 0 || wDec <= 0 || curveType > 1|| wAcc >1|| wDec >1)
	{
		motion->errorCode = VEL_PARAM_ERROR;
	}
	else
	{
		motion->errorCode = NO_ERROR;
	}
}
void MovPVelFollow(Motion_ *motion, double feedbackPos, double targetVel)
{
	if (!motion->interruptFlag)
	{
		MovVelFollow_ *movVelFollow;
		movVelFollow = &motion->movVelFollow;
		if (motion->errorCode == VEL_PARAM_ERROR)
		{
			return 0;
		}
		if (motion->initFlag == 0)
		{
			movVelFollow->u_1 = 0;
			movVelFollow->u_2 = 0;
			movVelFollow->y_1 = 0;
			movVelFollow->y_2 = 0;
			motion->initFlag = 1;
			motion->out.pos = feedbackPos;
		}
		if (motion->initFlag == 1)
		{
			// motion->out.vel = 0.496679133402659*0.0001*OpenFollow->u_1 + 0.493378950789292*0.0001*OpenFollow->u_2 - (-1.980099667498336)*OpenFollow->y_1 - (0.980198673306755)*OpenFollow->y_2;
			motion->out.vel = 0.001209104274250*movVelFollow->u_1 + 0.001169464760281*movVelFollow->u_2 - (-1.902458849001428)*movVelFollow->y_1 - (0.904837418035960)*movVelFollow->y_2;
			motion->out.pos += motion->out.vel;
			movVelFollow->u_2 = movVelFollow->u_1;
			movVelFollow->y_2 = movVelFollow->y_1;
			movVelFollow->u_1 = targetVel;
			movVelFollow->y_1 = motion->out.vel;
		}
	}
}
int MovPSingleJoint(Motion_ *motion, double feedbackPos, double posTarget, double velRatio)
{
	if (!motion->interruptFlag)
	{
		int i;
		if (motion->errorCode == VEL_PARAM_ERROR)
		{
			return 0;
		}
		if (motion->initFlag == 0)
		{
			double TotalDis = fabs(posTarget - feedbackPos);
			motion->movP.posStart[0] = feedbackPos;
			if (posTarget - feedbackPos < 0)
			{
				motion->movP.dir = -1;
			}
			else if (posTarget - feedbackPos > 0)
			{
				motion->movP.dir = 1;
			}
			else
			{
				return 0;

			}
			//建立速度曲線
			SetDynamicInputParam(motion, TotalDis, velRatio);
			CreateVelCurve(motion);
			motion->out.pos = 0;
			motion->out.vel = 0;
			//初始化完畢
			motion->initFlag = 1;
		}
		if (motion->initFlag == 1)
		{
			int motionFinish;
			motionFinish = VelOutput(&motion->velParam);
			motion->out.pos = motion->movP.posStart[0] + motion->movP.dir*motion->velParam->out.pos;
			motion->out.vel = motion->movP.dir*motion->velParam->out.vel;
			for (i = 0; i < 3; i++)
			{
				motion->out.posVector[i] = 0;
				motion->out.velVector[i] = 0;
			}
			return motionFinish;
		}
	}
	motion->out.pos = feedbackPos;
	motion->out.vel = 0;
	return 0;
}
int MovPOpenFollow(Motion_ *motion, double feedbackPos, double targetPos, double targetVel, double lowVelLimitRatio, double accur)
{
	if (!motion->interruptFlag)
	{
		MovOpnenFollow_ *movOpenFollow;
		MovVelInput_ *MovVelIn;
		double offset = fabs(feedbackPos - targetPos);
		movOpenFollow = &motion->movOpenFollow;
		MovVelIn = &motion->movVelParam.in;
		targetVel = fabs(targetVel);
		if (targetVel >= MovVelIn->velMax)
		{
			targetVel = MovVelIn->velMax;
		}
		if (motion->initFlag == 0)
		{
			if (motion->errorCode == VEL_PARAM_ERROR)
			{
				return 0;
			}
			if (fabs(targetPos - feedbackPos) <= accur || fabs(targetVel) == 0.000)
			{
				motion->out.pos = feedbackPos;
				motion->out.vel = 0;
				return 1;
			}
			double boundAera = 0.5*(MovVelIn->accTime*MovVelIn->velMax + MovVelIn->decTime*MovVelIn->velMax) + targetVel * MovVelIn->decTime;
			if (targetPos - feedbackPos >= 0)
			{
				movOpenFollow->dir = 1;
			}
			else
			{
				movOpenFollow->dir = -1;
			}
			FreeCurvList(&motion->velParam);
			if (offset >= boundAera)
			{
				StartCurve(&motion->velParam);
				AddCurve(&motion->velParam, MovVelIn->accTime, MovVelIn->wAcc, offset / 2, MovVelIn->velMax, MovVelIn->curveType);
				AddCurve(&motion->velParam, MovVelIn->decTime, MovVelIn->wDec, offset / 2, targetVel, MovVelIn->curveType);
				EndCurve(&motion->velParam);
				movOpenFollow->targetVelPre = MovVelIn->velMax;
			}
			else
			{
				StartCurve(&motion->velParam);
				AddCurve(&motion->velParam, MovVelIn->accTime, MovVelIn->wAcc, offset, targetVel, MovVelIn->curveType);
				EndCurve(&motion->velParam);
				movOpenFollow->targetVelPre = targetVel;
			}
			motion->initFlag = 1;
			motion->out.pos = feedbackPos;
			movOpenFollow->posTargetPre = targetPos;
			motion->errorCode = NO_ERROR;
		}
		if (motion->initFlag == 1&& !motion->errorCode)
		{
			int updateVelCurve = 0;
			double targetVelChack;
			if (VelOutput(&motion->velParam) || (fabs(movOpenFollow->velPre - targetVel) > 0.001 && (offset > lowVelLimitRatio) && fabs(motion->velParam->out.vel - motion->velParam->calc.endVel) <= 0.00001))
			{
				updateVelCurve =1;
			}
			 targetVelChack = targetPos - movOpenFollow->posTargetPre;
			movOpenFollow->posTargetPre = targetPos;
			
			if (movOpenFollow->dir*(targetVelChack) < 0)
			{
				motion->errorCode = OPENFOLLOW_DIR_ERROR;
				motion->out.vel = 0;
				return 0;
			}
			if (updateVelCurve == 0)
			{
				motion->out.vel = movOpenFollow->dir*motion->velParam->out.vel;
				motion->out.pos += motion->out.vel;
				movOpenFollow->velPre = motion->out.vel;
				return 0;
			}
			else 
			{
				if (targetVel != 0.0 || fabs(motion->out.vel) > 0.001)
				{
					double accTime;
					if (fabs(targetPos - feedbackPos) <= accur)
					{
						motion->out.vel = 0;
						MotionChange(motion);
						return 1;

					}
					FreeCurvList(&motion->velParam);
					MovVelIn = &motion->movVelParam.in;
					StartCurve(&motion->velParam);
					if (fabs(targetVel - fabs(motion->out.vel))* MovVelIn->accTime / 2 > offset)
					{
						accTime = offset * 2 / fabs(targetVel - fabs(motion->out.vel));
					}
					else
					{
						accTime = MovVelIn->accTime;
					}
					if (accTime <= 10)
					{
						accTime = 10;
					}
					AddCurveWithInitVel(&motion->velParam, accTime, MovVelIn->wAcc, fabs(targetPos - feedbackPos), targetVel, fabs(movOpenFollow->velPre), MovVelIn->curveType);
					EndCurve(&motion->velParam);
					updateVelCurve = VelOutput(&motion->velParam);
					motion->out.vel = movOpenFollow->dir*motion->velParam->out.vel;
					motion->out.pos += motion->out.vel;
					movOpenFollow->velPre = motion->out.vel;
					movOpenFollow->targetVelPre = targetVel;
					return 0;
				}
				else
				{
					motion->out.vel = 0;
					MotionChange(motion);
					return 1;
				}
			}
		}
	}
	motion->out.pos = feedbackPos;
	motion->out.vel = 0;
	return 0;
}
int MotionStop(Motion_ *motion,double stopTime)
{
	if (!motion->motionStop.stopInitFlag)
	{	
		
		
		if (fabs(motion->out.vel) <= 0.00001)
		{
			return 1;
		}
		FreeCurvList(&motion->velParam);
		motion->initFlag = 0;
		MovVelInput_ *MovVelIn;
		MovVelIn = &motion->movVelParam.in;
		StartCurve(&motion->velParam);
		double stopDis = fabs(motion->out.vel)*stopTime / 2;
		AddCurveWithInitVel(&motion->velParam, 
							MovVelIn->decTime,
							MovVelIn->wAcc,
							stopDis,
							0, 
							fabs(motion->out.vel),
							MovVelIn->curveType
							);
		EndCurve(&motion->velParam);
		motion->motionStop.stopInitFlag = 1;
		if (motion->out.vel < 0)
		{
			motion->motionStop.dir = -1;
		}
		else 
		{
			motion->motionStop.dir = 1;
		}
	}
	if (motion->motionStop.stopInitFlag)
	{
		VelOutput(&motion->velParam);
		motion->out.vel = motion->motionStop.dir*motion->velParam->out.vel;
		motion->out.pos += motion->out.vel;
		if (fabs(motion->out.vel) >= 0.00001)
		{
			return 0;
		}
		else
		{
			motion->motionStop.stopInitFlag = 0;
			return 1;
		}

	}
}
int MovOPR(Motion_ *motion, double feedbackPos,double velRatio,double squirmVelRatio,double dogOnLength ,int dog,int dir)
{
	if (!motion->interruptFlag)
	{		
		if (motion->initFlag == 0)
		{
			if (dogOnLength <= 0)
			{
				return 0;
			}
			if (squirmVelRatio <= 0 || squirmVelRatio >= 1)
			{
				return 0;
			}
			if (velRatio > 1 || velRatio < 0)
			{
				return 0;
			}
			if (dog != 0)
			{
				return 0;
			}
				FreeCurvList(&motion->velParam);
				StartCurve(&motion->velParam);
				AddCurve(
							&motion->velParam,
							motion->movVelParam.in.accTime,
							motion->movVelParam.in.wAcc ,
							3.4*10000000000000000, 
							fabs(velRatio*motion->movVelParam.in.velMax),
							motion->movVelParam.in.curveType
						);
				EndCurve(&motion->velParam);
				motion->initFlag = 1;
				motion->movOPR.dogPre = 0;
				motion->movOPR.initFlagDog = 0;
				motion->movOPR.dogOnLength = dogOnLength;
				if (dir >= 1)
				{
					motion->movOPR.dir = 1;
				}
				else
				{
					motion->movOPR.dir = -1;
				}
				motion->out.pos = feedbackPos;
		}
		{
			if(motion->movOPR.dogPre == 0 && dog == 1)
			{
				 if (motion->movOPR.initFlagDog == 0)
				 {
					 motion->movOPR.squirmVel = squirmVelRatio * motion->out.vel;
					 FreeCurvList(&motion->velParam);
					 double dropArea = motion->movVelParam.in.decTime*(fabs(motion->out.vel) - fabs(motion->movOPR.squirmVel)) / 2;
					 double dropArea_1;
					 if (dropArea >= motion->movOPR.dogOnLength*0.5)
					 {
						 dropArea = motion->movOPR.dogOnLength*0.5;
					 }
					dropArea_1 = motion->movOPR.dogOnLength - dropArea;
					 AddCurveWithInitVel
					 (
						 &motion->velParam, 
						 motion->movVelParam.in.decTime,
						 motion->movVelParam.in.wDec,
						 dropArea,
						 fabs(motion->movOPR.squirmVel),
						 fabs(motion->out.vel), 
						 motion->movVelParam.in.curveType
					 );
					 double decTimeSquirm = motion->movOPR.squirmVel/motion->movVelParam.in.velMax*  motion->movVelParam.in.decTime;
					 AddCurve
					 (
						 &motion->velParam,
						 motion->movVelParam.in.decTime,
						 motion->movVelParam.in.wAcc,
						 dropArea_1,
						 0,
						 motion->movVelParam.in.curveType
					 );
					 EndCurve(&motion->velParam);
					 motion->movOPR.initFlagDog = 1;
				 }
				motion->movOPR.dogPre = 1;
			}
			int dogFinish;
			dogFinish=VelOutput(&motion->velParam);
			if (((motion->velParam->next==NULL&& motion->movOPR.initFlagDog==1)|| dogFinish))
			{
				if (motion->movOPR.dogPre == 1 && dog == 0)
				{
					motion->out.vel =0;
					return 1;
				}
			}
			motion->out.vel = motion->movOPR.dir*motion->velParam->out.vel;
			motion->out.pos += motion->out.vel;
			return 0;
		}
	}
	motion->out.pos = feedbackPos;
	motion->out.vel = 0;
	return 0;
}
